package models

import "time"

type Recipe struct {
	ID string `json:"id"`
	Name         string   `json:"name"`
	Tags         []string `json:"tags"`
	Ingredients  []string `json:"ingredients"`
	Instructions []string `json:"instructions"`
	PublishedAt  time.Time `json:"publishedAt"`
}

type EmptyDataError struct {
	Message string
}
type IdNotFoundError struct {
	Message string
}
type IdAlreadyFoundError struct {
	Message string
}
func NewEmptyDataError(message string) *EmptyDataError {
	return &EmptyDataError{Message: message}
}
func NewIdNotFoundError(message string) *IdNotFoundError {
	return &IdNotFoundError{Message: message}
}

func (ed *EmptyDataError) Error() string {
	return ed.Message
}
func (idNf *IdNotFoundError) Error() string {
	return idNf.Message
}