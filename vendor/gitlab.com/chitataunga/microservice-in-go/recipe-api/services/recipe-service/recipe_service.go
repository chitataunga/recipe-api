package recipe_service

import models "gitlab.com/chitataunga/microservice-in-go/recipe-api/models"


var recipesMap map[string]models.Recipe
func init() {
	recipesMap = make(map[string]models.Recipe)
}

func GetRecipes() []models.Recipe {
	n := len(recipesMap)
	var recipes []models.Recipe = make([]models.Recipe, 0, n)
	for _, v := range recipesMap {
		recipes = append(recipes, v)
	}
	return recipes
}
func GetRecipe(id string) (*models.Recipe, error) {
	n := len(recipesMap)
	if n == 0 {
		return nil, models.NewEmptyDataError("the storage hs no data yet. Please do some POST")
	}
	v, ok := recipesMap[id]
	if !ok {
		return nil, models.NewIdNotFoundError("item with id: " + id + " not found")
	} else {
		return &v, nil
	}
}
func DeleteRecipe(id string) (*models.Recipe, error) {
	v, ok := recipesMap[id]
	delete(recipesMap, id)
	if !ok {
		return nil, models.NewIdNotFoundError("item with id: " + id + " not found")
	} else {
		
		return &v, nil
	}
}
// UpdateRecipe returns not found if id not exist
func UpdateRecipe(id string, recipe models.Recipe) (*models.Recipe, error) {
	v, ok := recipesMap[id]
	if !ok {
		return nil, models.NewIdNotFoundError("item with id: " + id + " not found")

	} else {
		recipe.ID = id
		recipesMap[id] = recipe
		return &v, nil
	}
}
func Add(id string, recipe models.Recipe) *models.Recipe {
	recipesMap[id] = recipe
	v := recipesMap[id]
	return &v
}
