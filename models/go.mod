module gitlab.com/chitataunga/microservice-in-go/recipe-api/models

go 1.18

replace gitlab.com/chitataunga/microservice-in-go/recipe-api/models => ./models
