package main

import (
	"log"
	"net/http"
	"net/http/httputil"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/rs/xid"
	models "gitlab.com/chitataunga/microservice-in-go/recipe-api/models"
	service "gitlab.com/chitataunga/microservice-in-go/recipe-api/services/recipe-service"
)

func NewRecipeHandler(c *gin.Context) {
	var recipe models.Recipe
	if err := c.ShouldBindJSON(&recipe); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}
	recipe.ID = xid.New().String()
	recipe.PublishedAt = time.Now()
	rec := service.Add(recipe.ID, recipe) 
	c.JSON(http.StatusOK, *rec)
}
func ListRecipesHandler(c *gin.Context) {
	c.JSON(http.StatusOK, service.GetRecipes())
}
func UpdateRecipeHandler(c *gin.Context){
	id := c.Param("id")
	var recipe models.Recipe
	httputil.DumpRequest(c.Request, true)
	log.Println("executing put for id: ", id)
	err := c.ShouldBindJSON(&recipe)
	if err != nil {
		 c.JSON(http.StatusNotFound, gin.H{
			"error": err.Error()})
			return
	}
	uspert,_ := service.UpdateRecipe(id, recipe)
	c.JSON(http.StatusOK, *uspert)

}


func init() {

}


func main() {
	router := gin.Default()
	router.POST("/recipes", NewRecipeHandler)
	router.GET("/recipes", ListRecipesHandler)
	router.PUT("/recipes/:id", UpdateRecipeHandler)
	router.Run(":9093")
	// contin 
	// https://subscription.packtpub.com/book/web-development/9781801074858/4/ch04lvl1sec16/implementing-http-routes
}