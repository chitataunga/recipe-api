module gitlab.com/chitataunga/microservice-in-go/recipe-api/services/recipe_service

go 1.18

replace gitlab.com/chitataunga/microservice-in-go/recipe-api/models => ../../models

require gitlab.com/chitataunga/microservice-in-go/recipe-api/models v0.0.0-00010101000000-000000000000
